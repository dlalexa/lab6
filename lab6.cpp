/***************************
David Alexander 
*CPSC 1021, 001, F20
dlalexa@g.clemson.edu
Nushrat Humaira
**************************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{     //struct used for assigning values for each employee
  string lastName;
  string firstName;
  int birthYear;
  double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);     //declaring the function that helps sort an array
int myrandom (int i) { return rand()%i;}                       //myrandom also used to help in the sorting of the array

int main(int argc, char const *argv[]) {  

  srand(unsigned (time(0)));

  struct Employee employees[10];            //array of the struct Employee
  
  cout << "Create 10 employees." << endl;   //prompting the user to create 10 employees

  for(int i = 0;i < 10; i++){      //for loop to repeat creation of employees
    cout << "Enter the last name of the employee." << endl;   //prompting the user to enter the name for each of the last names
    cin >> employees[i].lastName;

    cout << "Enter the first name of the employee." << endl;  //prompting the user to enter the name for each of the last names
    cin >> employees[i].firstName;

    cout << "Enter the birthyear of the employee." << endl;   //prompting the user to enter the birth year of each employee
    cin >> employees[i].birthYear;

    cout << "Enter the hourly wage of the employee." << endl;  //prompting the user to enter the hourly wage of each employee
    cin >> employees[i].hourlyWage;
  }

  random_shuffle(begin(employees), end(employees)+1, myrandom); //randomly shuffles all entries in the array of employees 

  struct Employee arrEmp[5] {employees[0],employees[1],employees[2],employees[3],employees[4]}; //array that gets the first five entries of the randomly sorted array

  sort( arrEmp, arrEmp + 5, &name_order);   //sorts the array in by last name in alphabetical order using the name_order function
  
  for(int i = 0; i < 5; i++){   //for loop that prints out the new array
    cout << setw(5) << arrEmp[i].lastName <<", "<< arrEmp[i].firstName << endl;  //prints out the first and last names of the employees in the format of "Last, First"
    cout << setw(5) << arrEmp[i].birthYear << endl;   //prints out the birth year of the employees
    cout << setw(5) << arrEmp[i].hourlyWage << endl;  //prints out the hourly wage of the employees
  }
  
  return 0;
  }
bool name_order(const employee& lhs, const employee& rhs) {      //name_order function used for sorting the array 
    if(lhs.lastName < rhs.lastName)
      return true;
    else if(lhs.lastName > rhs.lastName)       //it will only be true if the last name of one employee is greater than the other
      return false;
      
      return false;
  }
